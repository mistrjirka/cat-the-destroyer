extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var fileMan = File.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	fileMan.open("user://score.save", File.READ)
	var score = {} 
	score = parse_json(fileMan.get_as_text())
	print(score)
	self.set_text(String(score["score"]))
