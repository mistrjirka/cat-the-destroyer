extends Control

onready var fileMan = File.new()

func _ready():
	if not fileMan.file_exists("user://score.save"):
		resetScore()
	setScore()

func resetScore():
	var score = fileMan.open("user://score.save", File.WRITE)
	fileMan.store_string(to_json({"score": 0}))
	setScore()
	
func setScore():
	fileMan.open("user://score.save", File.READ)
	var score = {} 
	score = parse_json(fileMan.get_as_text())
	get_node("Label/score").set_text(String(score["score"]))
	fileMan.close()

func _on_Quit_pressed():
	get_tree().quit()

func _on_Start_pressed():
	get_tree().change_scene("res://LevelSelect.tscn")
