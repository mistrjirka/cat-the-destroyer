extends KinematicBody2D

#constansts
const WALK_SPEED = 200
const JUMP_SPEED = -220
const GRAVITY = 500
const floor_normal = Vector2(0,-1)
#changable variables
onready var velocity = Vector2()
onready var allowedToJump = is_on_floor()
onready var wasOnTheFloor = is_on_floor()
onready var timer
onready var jumped = false

func allowToJump():
	allowedToJump = false
	timer.stop()

func _physics_process(delta):
	#gravity
	velocity.y += delta * GRAVITY
	#apply
	var motion = velocity * delta
	var collision = move_and_collide(motion)
	if collision:
		move_and_slide(collision.normal)
	velocity = move_and_slide(velocity, floor_normal)
	handleInput()
	if is_on_floor():
		allowedToJump = true
	
	if not is_on_floor() and wasOnTheFloor:
		timer = Timer.new()
		timer.connect("timeout",self,"allowToJump")
		timer.set_wait_time(0.15)
		add_child(timer) #to process
		timer.start() #to start
	
	if allowedToJump:
		canJump()
	else:
		jumped = false
	wasOnTheFloor = is_on_floor()


func canJump():
	jumped = true
	if Input.is_action_pressed("jump"):
		velocity.y = JUMP_SPEED

func handleInput():
	velocity.x = velocity.x - (velocity.x/2)
	if Input.is_action_pressed("right"):
		velocity.x = WALK_SPEED
	if Input.is_action_pressed("left"):
		velocity.x = -WALK_SPEED
