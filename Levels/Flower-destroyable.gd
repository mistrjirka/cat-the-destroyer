extends RigidBody2D

const BREAKING_SPEED = 100
const cost = 300
onready var fileMan = File.new()
func _ready():
	contact_monitor = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var speedV2 = self.linear_velocity
	var speed = sqrt(speedV2.x*speedV2.x + speedV2.y*speedV2.y)
	if speed >= BREAKING_SPEED and len(get_colliding_bodies ( )) != 0:
		setScore(cost)

func setScore(value):
	fileMan.open("user://score.save", File.READ_WRITE)
	var score = to_json({"score": value + float(parse_json(fileMan.get_as_text())["score"])})
	fileMan.store_string(score)
	print(score)
	get_node("../player2/GUI/Label/score").set_text(String(parse_json(fileMan.get_as_text())["score"]))
	fileMan.close()
