extends ItemList

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var path = "res://Levels/"
onready var Levels = list_files_in_directory(path)
# Called when the node enters the scene tree for the first time.
func _ready():

	
	print(Levels)
	for i in Levels:
		self.add_item(i.split(".",true)[0], null, true)
	# Instance as child:
	#var scene_resource = load(path)
	#var scene = scene_resource.instance()
	#self.add_child(scene)
	
	# Or, load as complete replacement of current scene:
	#get_tree().change_scene(path)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func list_files_in_directory(path):
    var files = []
    var dir = Directory.new()
    dir.open(path)
    dir.list_dir_begin()

    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with(".") and ".tscn" in file:
            files.append(file)

    dir.list_dir_end()
    return files



func _on_Exit_pressed():
	print("ahoj2")
	if not len(self.get_selected_items()) == 0:
		var level = path + Levels[self.get_selected_items()[0]]
		get_tree().change_scene(level)
